import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar


plugins {
    val kotlinVersion by System.getProperties()
    val shadowVersion by System.getProperties()

    id("org.jetbrains.kotlin.jvm") version "$kotlinVersion"
    id("java")
    id("com.github.johnrengelman.shadow") version "$shadowVersion"
}

repositories {
    mavenCentral()
}

tasks.test {
    // Use the built-in JUnit support of Gradle.
    useJUnitPlatform()
}

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "com.alexshimmy.mqpinger.AppKt"
    }

    archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
}


dependencies {

    val exposedVersion = "0.36.1"
    val ktorVersion = "1.6.7"
    val junitVersion = "5.8.2"
    val hivemqVersion = "1.3.0"
    val kotlinxCoroutinesVersion = "1.5.2"
    val sqliteJdbcVersion = "3.36.0.3"
    val janinoVersion = "3.1.6"
    val logbackVersion = "1.2.8"
    val slf4jApiVersion = "1.7.32"
    val kotlinVersion by System.getProperties()
    val koinVersion = "3.1.4"

    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    implementation("com.hivemq:hivemq-mqtt-client:$hivemqVersion")

    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-jackson:$ktorVersion")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")

    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.xerial:sqlite-jdbc:$sqliteJdbcVersion")
    //Logging
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion")
    implementation("ch.qos.logback:logback-core:$logbackVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("org.codehaus.janino:janino:$janinoVersion") // ScriptEvaluator (logs filtering)

    implementation("io.insert-koin:koin-ktor:$koinVersion")
}
