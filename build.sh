#!/usr/bin/env bash

./gradlew clean shadowJar

java -DMQTT_HOST_IP=$MQTT_HOST_IP -DMQTT_PORT=$MQTT_PORT -DMQTT_WEBSOCKET_PORT=$MQTT_WEBSOCKET_PORT \
    -DMQTT_TOPIC=$MQTT_TOPIC -DHTTP_PORT=$HTTP_PORT -jar ./build/libs/MQPinger.jar
