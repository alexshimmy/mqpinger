package com.alexshimmy.mqpinger

import com.alexshimmy.mqpinger.pojo.TelegramConfiguration
import com.alexshimmy.mqpinger.sqlite.tables.TelegramConfigTable.apiToken
import com.alexshimmy.mqpinger.sqlite.tables.TelegramConfigTable.chatId
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.slf4j.LoggerFactory

class TelegramManager(private val tgConfig: TelegramConfiguration) {

    private val logger = LoggerFactory.getLogger(this.javaClass.simpleName)
    private val client = HttpClient(CIO)

    suspend fun sendMessage(text: String): Boolean {

        if (!isValidMessageData(tgConfig.chatId, tgConfig.apiToken, text)) {
            logger.warn("Telegram data is invalid. Chat id: ${tgConfig.chatId}, " +
                    "API token length: ${tgConfig.apiToken.length}, Text: $text")

            return false
        }

        try {

            val response: HttpResponse = client.get(
                "https://api.telegram.org/bot$apiToken/sendMessage?chat_id=$chatId&text=\"$text\""
            )

            when (response.status.isSuccess()) {
                true -> logger.info("Message '$text' has been sent to Telegram chat with id '$chatId'")
                false -> logger.error("Unable to send message to Telegram: $response")
            }

            return response.status.isSuccess()
        } catch (e: ClientRequestException) {
            logger.error("Unable to send message to Telegram: $e")

            return false
        }
    }

    private fun isValidMessageData(chatId: String, apiToken: String, text: String): Boolean {
        return chatId.isNotEmpty() && apiToken.isNotEmpty() && text.isNotEmpty()
    }
}