package com.alexshimmy.mqpinger

import org.slf4j.LoggerFactory
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

object HostUtils {

    private const val VALID_IP_ADDRESS_REGEXP = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
    private const val VALID_HOSTNAME_REGEXP = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])"
    private const val VALID_PORT_REGEXP = "([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$"

    private val logger = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun isHostReachable(host: String, timeout: Int = 5000): Boolean {
        return if (host.contains(":")) {
            try {
                val socketAddress: SocketAddress = InetSocketAddress(
                    host.substringBefore(":"), // IP address
                    host.substringAfter(":").toInt() // Port
                )
                val sock = Socket()
                sock.connect(socketAddress, timeout)
                sock.close()

                true
            } catch (e: Exception) {
                logger.error(e.localizedMessage)

                false
            }
        } else {
            return try {
                InetAddress.getByName(host).isReachable(timeout)
            } catch (e: Exception) {
                logger.error(e.localizedMessage)

                false
            }
        }
    }

    fun isHostValid(host: String): Boolean {
        val isHostWithPort = host.contains(":")
        val portRegexp = if (isHostWithPort) ":$VALID_PORT_REGEXP" else ""
        return "$VALID_HOSTNAME_REGEXP$portRegexp".toRegex().matches(host) ||
                "$VALID_IP_ADDRESS_REGEXP$portRegexp".toRegex().matches(host)
    }
}
