package com.alexshimmy.mqpinger

import com.hivemq.client.mqtt.datatypes.MqttQos
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client
import java.util.*

object MQTTClient {
    fun configureMQTTClient(mqttBrokerHost: String, mqttBrokerPort: Int): Mqtt5BlockingClient {
        return Mqtt5Client.builder()
            .identifier("MQTTClient_${UUID.randomUUID()}")
            .serverHost(mqttBrokerHost)
            .serverPort(mqttBrokerPort)
            .buildBlocking()
    }
}

fun Mqtt5BlockingClient.sendMessage(topic: String, message: String, qos: MqttQos = MqttQos.AT_LEAST_ONCE) {
    this.publishWith().topic(topic).qos(qos).payload(message.toByteArray()).send()
}
