package com.alexshimmy.mqpinger.sqlite

import com.alexshimmy.mqpinger.pojo.HostAddress
import com.alexshimmy.mqpinger.pojo.TelegramConfiguration
import com.alexshimmy.mqpinger.sqlite.tables.HostsTable
import com.alexshimmy.mqpinger.sqlite.tables.TelegramConfigTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory

object SQLiteManager {

    private val logger = LoggerFactory.getLogger("SQLiteManager")

    init {
        Database.connect("jdbc:sqlite:sqlite.db", driver = "org.sqlite.JDBC")
        transaction {
            addLogger(Slf4jSqlDebugLogger)
            SchemaUtils.create(HostsTable, TelegramConfigTable)

            if (isTableEmpty(HostsTable)) {
                val defaultHostConfiguration = HostAddress()

                HostsTable.insert {
                    it[address] = "localhost"
                    it[pauseBetweenConnections] = defaultHostConfiguration.pauseBetweenConnections
                    it[hostConnectionTimeout] = defaultHostConfiguration.hostConnectionTimeout

                    logger.info("HostsTable has been initialized")
                }
            }

            if (isTableEmpty(TelegramConfigTable)) {
                logger.info("Telegram configuration is empty")

                TelegramConfigTable.insert {
                    val defaultConfig = TelegramConfiguration()
                    it[chatId] = defaultConfig.chatId
                    it[apiToken] = defaultConfig.apiToken

                    logger.info("Telegram configuration table has been initialized with default empty values")
                }
            }
        }
    }

    fun deleteHost(address: String): Boolean {
        return try {
            transaction {
                // delete host if exists
                HostsTable.deleteWhere { HostsTable.address like address }
            }

            true
        } catch (e: Exception) {
            logger.error(e.localizedMessage)
            false
        }
    }

    fun addHost(host: HostAddress): Boolean {
        return try {
            transaction {
                // add host if not exists
                getHostBy(host.address) ?: HostsTable.insert {
                    logger.info("New host has been added: $host")
                    it[address] = host.address
                    it[hostConnectionTimeout] = host.hostConnectionTimeout
                    it[pauseBetweenConnections] = host.pauseBetweenConnections
                }
            }

            true
        } catch (e: Exception) {
            logger.error(e.localizedMessage)
            false
        }
    }

    fun getAllHostAddresses(): List<HostAddress> {
        lateinit var hosts: List<HostAddress>
        transaction {
            hosts = HostsTable.selectAll().map {
                HostAddress(
                    it[HostsTable.address],
                    it[HostsTable.pauseBetweenConnections],
                    it[HostsTable.hostConnectionTimeout]
                )
            }
        }

        return hosts
    }

    private fun getHostBy(address: String): String? {
        return HostsTable.selectAll().firstOrNull { it[HostsTable.address] == address }.apply {
            logger.debug("Host address $address not found")
        }?.toString()
    }

    private fun isTableEmpty(table: Table): Boolean {
        return table.selectAll().firstOrNull() == null
    }

    fun updateTelegramConfiguration(chatId: String, apiToken: String): Boolean {
        return try {
            transaction {
                TelegramConfigTable.update {
                    it[TelegramConfigTable.chatId] = chatId
                    it[TelegramConfigTable.apiToken] = apiToken

                    logger.info("Telegram config has been updated")
                }
            }

            true
        } catch (e: Exception) {
            logger.error(e.localizedMessage)

            false
        }
    }

    fun getTelegramConfiguration(): TelegramConfiguration {
        return try {
            lateinit var resultRow: ResultRow
            transaction {
                resultRow = TelegramConfigTable.selectAll().first()
            }

            TelegramConfiguration(
                resultRow[TelegramConfigTable.chatId],
                resultRow[TelegramConfigTable.apiToken]
            )
        } catch (e: NoSuchElementException) {
            logger.debug("Telegram configuration is not available. ${e.localizedMessage}")

            TelegramConfiguration()
        }
    }
}
