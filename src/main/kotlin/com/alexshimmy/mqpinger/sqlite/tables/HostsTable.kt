package com.alexshimmy.mqpinger.sqlite.tables

import org.jetbrains.exposed.sql.Table

object HostsTable : Table("HostsTable") {

    private val id = integer("id").autoIncrement()
    override val primaryKey = PrimaryKey(id, name = "PK_Hosts_ID")

    val address = varchar("address", 50)
    val pauseBetweenConnections = integer("pauseBetweenConnections")
    val hostConnectionTimeout = integer("hostConnectionTimeout")
}
