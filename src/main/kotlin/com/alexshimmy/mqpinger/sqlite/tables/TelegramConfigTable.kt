package com.alexshimmy.mqpinger.sqlite.tables

import org.jetbrains.exposed.sql.Table

object TelegramConfigTable : Table("TelegramConfigTable") {

    private val id = integer("id").default(0)
    override val primaryKey = PrimaryKey(id, name = "PK_TelegramConfig_ID")

    val chatId = varchar("chatId", 50)
    val apiToken = varchar("apiToken", 50)
}
