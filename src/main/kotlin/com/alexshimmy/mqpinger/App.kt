package com.alexshimmy.mqpinger

import com.alexshimmy.mqpinger.api.HttpRequestManager
import com.alexshimmy.mqpinger.api.routes.*
import com.alexshimmy.mqpinger.pojo.HostAddress
import com.alexshimmy.mqpinger.pojo.PingMessage
import com.alexshimmy.mqpinger.pojo.TelegramConfiguration
import com.alexshimmy.mqpinger.sqlite.SQLiteManager
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.content.*
import io.ktor.jackson.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.slf4j.LoggerFactory


private val logger = LoggerFactory.getLogger("Appkt")

private val mapper = ObjectMapper()
private val client = MQTTClient.configureMQTTClient(Properties.mqttHost, Properties.mqttPort)
private val tgConfig: TelegramConfiguration
    get() { return SQLiteManager.getTelegramConfiguration() }
private val telegramManager: TelegramManager
    get() { return TelegramManager(tgConfig) }

private val singleModule = module {
    single { HttpRequestManager() }
}

fun main() {

    embeddedServer(Netty, port = Properties.httpPort) {
        routing {
            install(ContentNegotiation) {
                jackson()
            }
            install(CallLogging)
            install(Koin) {
                modules(singleModule)
            }

            static("/") {
                resources("static")
                defaultResource("static/index.html")
            }

            addHost()
            deleteHost()

            getMQTTConfiguration()

            changeTelegramConfiguration()
            getTelegramConfiguration()
        }

        if (!client.state.isConnected) {
            client.connect()
        }

        for (host in SQLiteManager.getAllHostAddresses()) {
            launch { runHostMonitoring(host) }
        }
    }.start(wait = true)
}

suspend fun runHostMonitoring(host: HostAddress) {
    logger.info("Run monitoring for host $host")
    while (SQLiteManager.getAllHostAddresses().contains(host)) {
        val isHostReachable = HostUtils.isHostReachable(host.address, host.hostConnectionTimeout)

        if (!isHostReachable) {
            val tgText = "Host $host is not reachable"
            telegramManager.sendMessage(tgText)
        }

        val message = mapper.writeValueAsString(
            PingMessage(host.address, isHostReachable, System.currentTimeMillis())
        )
        if (SQLiteManager.getAllHostAddresses().contains(host)) { // host may be removed already
            client.sendMessage(Properties.mqttTopic, message)
        }

        delay(host.pauseBetweenConnections.toLong())
    }
    logger.info("Unable to find host ${host.address} in database. Monitoring is finished.")
}