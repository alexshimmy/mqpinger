package com.alexshimmy.mqpinger

import org.slf4j.LoggerFactory

object Properties {

    private val logger = LoggerFactory.getLogger("Properties")
    private val properties = System.getProperties()

    private const val MQTT_HOST_IP_DEFAULT = "0.0.0.0"
    private const val MQTT_HOST_IP_VARIABLE = "MQTT_HOST_IP"

    private const val MQTT_PORT_DEFAULT = "1883"
    private const val MQTT_PORT_VARIABLE = "MQTT_PORT"

    private const val MQTT_WEBSOCKET_PORT_DEFAULT = "8083"
    private const val MQTT_WEBSOCKET_PORT_VARIABLE = "MQTT_WEBSOCKET_PORT"

    private const val HTTP_PORT_DEFAULT = "8765"
    private const val HTTP_PORT_VARIABLE = "HTTP_PORT"

    private const val MQTT_TOPIC_DEFAULT = "mqpinger"
    private const val MQTT_TOPIC_VARIABLE = "MQTT_TOPIC"

    val mqttHost: String = properties.getProperty(MQTT_HOST_IP_VARIABLE, MQTT_HOST_IP_DEFAULT).also {
        logger.info("$MQTT_HOST_IP_VARIABLE: $it")
    }

    val mqttPort: Int = try {
        properties.getProperty(MQTT_PORT_VARIABLE, MQTT_PORT_DEFAULT).toInt().also {
            logger.info("${MQTT_PORT_VARIABLE}: $it")
        }
    } catch (e: NumberFormatException) {
        logger.warn("Unable to cast $MQTT_PORT_VARIABLE to Int. Default value: $MQTT_PORT_DEFAULT")

        MQTT_PORT_DEFAULT.toInt()
    }

    val mqttWebSocket: Int = try {
        properties.getProperty(MQTT_WEBSOCKET_PORT_VARIABLE, MQTT_WEBSOCKET_PORT_DEFAULT).toInt().also {
            logger.info("${MQTT_WEBSOCKET_PORT_VARIABLE}: $it")
        }
    } catch (e: NumberFormatException) {
        logger.warn("Unable to cast $MQTT_WEBSOCKET_PORT_VARIABLE to Int. Default value: $MQTT_WEBSOCKET_PORT_DEFAULT")

        MQTT_WEBSOCKET_PORT_DEFAULT.toInt()
    }

    val httpPort: Int = try {
        properties.getProperty(HTTP_PORT_VARIABLE, HTTP_PORT_DEFAULT).toInt().also {
            logger.info("${HTTP_PORT_VARIABLE}: $it")
        }
    } catch (e: NumberFormatException) {
        logger.warn("Unable to cast $HTTP_PORT_VARIABLE to Int. Default value: $HTTP_PORT_DEFAULT")

        HTTP_PORT_DEFAULT.toInt()
    }

    val mqttTopic: String = properties.getProperty(MQTT_TOPIC_VARIABLE, MQTT_TOPIC_DEFAULT).also {
        logger.info("${MQTT_TOPIC_VARIABLE}: $it")
    }
}
