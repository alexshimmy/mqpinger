package com.alexshimmy.mqpinger.api

abstract class HttpResponse {
    abstract val message: String
    abstract val success: Boolean
    abstract val status: Int
    abstract val timestamp: Long
}
