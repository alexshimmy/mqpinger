package com.alexshimmy.mqpinger.api

object HttpResponseFactory {

    fun createFailureResponse(message: String, status: Int): HttpResponse {
        return createResponse(message, status, false)
    }

    fun createSuccessResponse(message: String, status: Int): HttpResponse {
        return createResponse(message, status, true)
    }

    private fun createResponse(message: String = "", status: Int, success: Boolean): HttpResponse {
        return if (success) {
            SuccessHttpResponse(message, status)
        } else {
            FailureHttpResponse(message, status)
        }
    }
}
