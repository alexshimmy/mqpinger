package com.alexshimmy.mqpinger.api.routes

import com.alexshimmy.mqpinger.api.HttpRequestManager
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Route.getMQTTConfiguration() {
    val httpManager: HttpRequestManager by inject()

    get("mqtt/configuration") {
        httpManager.getMqttConfiguration(call)
    }
}
