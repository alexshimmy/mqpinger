package com.alexshimmy.mqpinger.api.routes

import com.alexshimmy.mqpinger.api.HttpRequestManager
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Route.changeTelegramConfiguration() {
    val httpManager: HttpRequestManager by inject()

    put("telegram/configuration") {
        httpManager.changeTelegramConfiguration(call)
    }
}

fun Route.getTelegramConfiguration() {
    val httpManager: HttpRequestManager by inject()

    get("telegram/configuration") {
        httpManager.getTelegramConfiguration(call)
    }
}
