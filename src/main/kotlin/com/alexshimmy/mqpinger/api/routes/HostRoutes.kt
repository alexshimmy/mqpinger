package com.alexshimmy.mqpinger.api.routes

import com.alexshimmy.mqpinger.api.HttpRequestManager
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject


fun Route.addHost() {
    val httpManager: HttpRequestManager by inject()

    post("host") {
        httpManager.addHost(call)
    }
}

fun Route.deleteHost() {
    val httpManager: HttpRequestManager by inject()

    delete("host") {
        httpManager.deleteHost(call)
    }
}
