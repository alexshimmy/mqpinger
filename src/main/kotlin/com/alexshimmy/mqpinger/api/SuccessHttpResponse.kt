package com.alexshimmy.mqpinger.api

class SuccessHttpResponse(override val message: String, override val status: Int) : HttpResponse() {
    override val success = true
    override val timestamp = System.currentTimeMillis()
}
