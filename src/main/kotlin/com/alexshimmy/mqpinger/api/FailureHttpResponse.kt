package com.alexshimmy.mqpinger.api

class FailureHttpResponse(override val message: String, override val status: Int) : HttpResponse() {
    override val success = false
    override val timestamp = System.currentTimeMillis()
}
