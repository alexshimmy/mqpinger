package com.alexshimmy.mqpinger.api

import com.alexshimmy.mqpinger.HostUtils
import com.alexshimmy.mqpinger.Properties
import com.alexshimmy.mqpinger.pojo.HostAddress
import com.alexshimmy.mqpinger.pojo.MQTTConfiguration
import com.alexshimmy.mqpinger.pojo.TelegramConfiguration
import com.alexshimmy.mqpinger.runHostMonitoring
import com.alexshimmy.mqpinger.sqlite.SQLiteManager
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*

class HttpRequestManager {

    suspend fun addHost(call: ApplicationCall) {
        val responseMessage: String
        val host: HostAddress
        try {
            host = call.receive()
        } catch (e: InvalidFormatException) {
            responseMessage = "Unable to add new host. Invalid data format"
            call.respond(HttpResponseFactory.createFailureResponse(message = responseMessage, status = 400))

            return
        }

        if (host.hostConnectionTimeout !in 1000..100000 || host.pauseBetweenConnections !in 1000..100000) {
            responseMessage = "Host connection timeout and pause between connection should be in 1_000..100_000"
            call.respond(HttpResponseFactory.createFailureResponse(message = responseMessage, status = 400))

            return
        }

        if (!HostUtils.isHostValid(host.address)) {
            responseMessage = "Host address ${host.address} is invalid"
            call.respond(HttpResponseFactory.createFailureResponse(message = responseMessage, status = 400))

            return
        }

        val result = SQLiteManager.addHost(host)
        if (result) {
            responseMessage = "New host ${host.address} has been added"
            runHostMonitoring(host)
            call.respond(HttpResponseFactory.createSuccessResponse(message = responseMessage, status = 200))
        } else {
            responseMessage = "Unable to add ${host.address}"
            call.respond(HttpResponseFactory.createFailureResponse(message = responseMessage, status = 500))
        }
    }

    suspend fun deleteHost(call: ApplicationCall) {

        val hostAddress = call.receive<HostAddress>().address
        val result = SQLiteManager.deleteHost(hostAddress)
        if (result) {
            val message = "Host $hostAddress has been deleted"
            call.respond(HttpResponseFactory.createSuccessResponse(message = message, status = 200))
        } else {
            call.respond(HttpResponseFactory.createFailureResponse(message = "Unable to delete the host", status = 500))
        }
    }

    suspend fun getTelegramConfiguration(call: ApplicationCall) {

        val telegramConfiguration = SQLiteManager.getTelegramConfiguration()
        call.respond(telegramConfiguration)
    }

    suspend fun changeTelegramConfiguration(call: ApplicationCall) {

        val requestData = call.receive<TelegramConfiguration>()
        val chatId = requestData.chatId
        val apiKey = requestData.apiToken

        val result = SQLiteManager.updateTelegramConfiguration(chatId, apiKey)

        if (result) {
            val message = "Telegram configuration has been changed"
            call.respond(HttpResponseFactory.createSuccessResponse(message = message, status = 200))
        } else {
            val message = "Unable to change Telegram configuration"
            call.respond(HttpResponseFactory.createFailureResponse(message = message, status = 500))
        }
    }

    suspend fun getMqttConfiguration(call: ApplicationCall) {

        val mqttConfiguration = MQTTConfiguration(
            Properties.mqttHost,
            Properties.mqttPort,
            Properties.mqttWebSocket,
            Properties.mqttTopic
        )

        if (mqttConfiguration.mqttBrokerHost.isNotEmpty()) {
            call.respond(mqttConfiguration)
        } else {
            val message = "Unable to get MQTT configuration"
            call.respond(HttpResponseFactory.createFailureResponse(message = message, status = 500))
        }
    }
}
