package com.alexshimmy.mqpinger.pojo

data class PingMessage(
    val targetHost: String,
    val isReachable: Boolean,
    val timestamp: Long
)
