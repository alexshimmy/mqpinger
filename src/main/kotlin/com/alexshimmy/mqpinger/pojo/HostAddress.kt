package com.alexshimmy.mqpinger.pojo

data class HostAddress(
    val address: String = "localhost",
    val pauseBetweenConnections: Int = 15000,
    val hostConnectionTimeout: Int = 5000
)
