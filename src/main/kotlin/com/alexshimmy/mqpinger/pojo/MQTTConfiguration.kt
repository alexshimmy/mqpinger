package com.alexshimmy.mqpinger.pojo

data class MQTTConfiguration(
    val mqttBrokerHost: String,
    val mqttBrokerPort: Int,
    val mqttWebSocketPort: Int,
    val mqttBrokerTopic: String
)
