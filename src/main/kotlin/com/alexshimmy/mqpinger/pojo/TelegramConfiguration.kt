package com.alexshimmy.mqpinger.pojo

data class TelegramConfiguration(val chatId: String = "", val apiToken: String = "")
