function startConnect() {
    // Generate a random client ID
    const clientID = "MQPinger-" + parseInt((Math.random() * 100).toString())

    getMQTTConfiguration(function (response) {
        let host = response.mqttBrokerHost
        let port = response.mqttWebSocketPort
        let topic = response.mqttBrokerTopic

        updateConfigStatus('Connecting to: ' + host + ' on port: ' + port)

        // Initialize new Paho client connection
        client = new Paho.MQTT.Client(host, Number(port), clientID)

        // Set callback handlers
        client.onConnectionLost = onConnectionLost
        client.onMessageArrived = onMessageArrived

        // Connect the client, if successful, call onConnect function
        client.connect({
            onSuccess: function () {
                onConnect(topic)
            }
        })
    })
}

// Called when the MQTT client connects
function onConnect(topic) {
    client.subscribe(topic)
    updateConfigStatus("Subscribed to: " + topic)
    updateConnectionStatus(true)
}

function onConnectionLost(responseObject) {
    console.log("onConnectionLost: Connection Lost")
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost: " + responseObject.errorMessage)
    }
    updateConnectionStatus(false)
}

function onMessageArrived(message) {
    let pingMessage = JSON.parse(message.payloadString)
    updatePage(pingMessage)
}

// Called when the disconnection button is pressed
function startDisconnect() {
    client.disconnect()
    updateConfigStatus("Disconnected")
    updateConnectionStatus(false)
}

function isNotNullAndNotUndefined(element) {
    return (typeof (element) != "undefined" && element != null)
}

function updatePage(pingMessage) {
    let messageId = pingMessage.targetHost
    let existingPingView = document.getElementById(messageId)
    if (isNotNullAndNotUndefined(existingPingView)) {
        existingPingView.querySelector('.target-host').innerHTML = pingMessage.targetHost
        existingPingView.querySelector('.date').innerHTML = getFormattedDateByTimestamp(pingMessage.timestamp)
        existingPingView.querySelector('.reachable').innerHTML = getReachableViewByMessage(pingMessage)
    } else {
        let table = document.getElementById("host-table")
        let newPingView = generateViewByPingMessage(pingMessage)

        let row = table.insertRow()
        row.className = "bg-blue" // set row background
        row.id = messageId
        row.innerHTML = newPingView

        table.insertRow().id = "spacing-row"
    }
}

function generateViewByPingMessage(message) {
    return `
        <td class="pt-3">
            <div class="pl-md-3 pl-1 name target-host">` + message.targetHost + `</div>
        </td>
        <td class="pt-3 mt-1 date">` + getFormattedDateByTimestamp(message.timestamp) + `</td>
        <td class="pt-3 reachable">` + getReachableViewByMessage(message) + `</td>
        <td class="pt-3 delete-button">
            <button type="button" class="btn btn-outline-danger btn-sm" onclick="deleteHost(this.parentElement.parentElement.id)">Delete</button>
        </td>
    `
}

function getReachableViewByMessage(message) {
    return message.reachable ? '<span class="fa fa-check pl-3"></span>' : '<span class="fa fa-times pl-3"></span>'
}

function getFormattedDateByTimestamp(timestamp) {
    let date = new Date(timestamp)

    return date.getDate() +
        "/" + (date.getMonth() + 1) +
        "/" + date.getFullYear() +
        " " + date.getHours() +
        ":" + date.getMinutes() +
        ":" + date.getSeconds()
}

function getMQTTConfiguration(callback) {
    $.ajax({
        url: document.URL + "mqtt/configuration",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: 'GET',
        success: callback,
        error: function (response) {
            console.log("Cannot receive MQTT configuration " + response.responseText)
            updateConfigStatus("Cannot receive MQTT configuration " + response.responseText)
        }
    })
}

function getTelegramConfiguration(callback) {
    $.ajax({
        url: document.URL + "telegram/configuration",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: 'GET',
        success: callback,
        error: function (response) {
            console.log("Cannot receive Telegram configuration " + response.responseText)
            updateConfigStatus("Cannot receive Telegram configuration " + response.responseText)
        }
    })
}

function saveTelegramConfiguration() {
    let apiToken = document.getElementById("api-token").value
    let chatId = document.getElementById("chat-id").value

    let data = JSON.stringify({
        "apiToken": apiToken,
        "chatId": chatId
    })

    $.ajax({
        url: document.URL + "telegram/configuration",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: data,
        type: 'PUT',
        success: function (response) {
            updateConfigStatus(response.message)
        },
        error: function (response) {
            console.log("Cannot update Telegram configuration " + response.responseText)
            updateConfigStatus("Cannot update Telegram configuration " + response.responseText)
        }
    })
}

function addHost() {
    let hostAddress = document.getElementById("host-address").value
    let hostConnectionTimeout = document.getElementById("host-connection-timeout").value
    let pauseBetweenConnections = document.getElementById("pause-between-connections").value

    let data = JSON.stringify({
        "address": hostAddress,
        "hostConnectionTimeout": hostConnectionTimeout,
        "pauseBetweenConnections": pauseBetweenConnections
    })

    $.ajax({
        url: document.URL + "host",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: data,
        type: 'POST',
        success: function (response) {
            updateHostStatus(response.message)
        },
        error: function (response) {
            console.log("Unable to add new host " + response.responseText)
            updateHostStatus("Unable to add new host " + response.responseText)
        }
    })
}

function deleteHost(rowId) {
    let hostRow = document.getElementById(rowId)
    let hostAddress = hostRow.querySelector('.target-host').innerHTML

    let data = JSON.stringify({
        "address": hostAddress
    })

    $.ajax({
        url: document.URL + "host",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: data,
        type: 'DELETE',
        success: function (response) {
            updateHostStatus(response.message)
            let spacingRow = hostRow.nextSibling
            hostRow.remove()
            spacingRow.remove()
        },
        error: function (response) {
            console.log("Unable to remove the host " + response.responseText)
            updateHostStatus("Unable to remove the host " + response.responseText)
        }
    })
}

function updateConfigStatus(message) {
    document.getElementById("messages").innerHTML = '<span class="text-secondary">' + message + '</span>'
}

function updateHostStatus(message) {
    document.getElementById("host-status").innerHTML = '<span class="text-secondary">' + message + '</span>'
}

function updateConnectionStatus(status) {
    if (status) {
        $("#connected-status").removeClass("d-none")
        $("#disconnected-status").addClass("d-none")
    } else {
        $("#connected-status").addClass("d-none")
        $("#disconnected-status").removeClass("d-none")
    }
}

$(function () { // when document is ready
    startConnect()

    getTelegramConfiguration(function (response) {
        if (isNotNullAndNotUndefined(response.apiToken) && isNotNullAndNotUndefined(response.chatId)) {
            document.getElementById("api-token").value = response.apiToken
            document.getElementById("chat-id").value = response.chatId
        } else {
            console.log(response)
        }
    })
})