# README #

### Run MQTT server ###
```MQTT_PORT=1883 MQTT_WEBSOCKET_PORT=8083 docker-compose up```

### Run App ###
```MQTT_HOST_IP=0.0.0.0 MQTT_PORT=1883 MQTT_WEBSOCKET_PORT=8083 MQTT_TOPIC=mqpinger HTTP_PORT=8765 sh build.sh```